﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class Kitapdüzenle : Form
    {
        Kitap g;
        public Kitapdüzenle(Kitap l)

        {
            InitializeComponent();
            g = l;
        }

        private void Kitapdüzenle_Load(object sender, EventArgs e)
        {
           
          


            textBox1.Text = g.ID.ToString();
            textBox2.Text = g.Adi;
            textBox3.Text = g.BasimYili.ToString();
            textBox4.Text = g.BasimYeriID.ToString();
            textBox5.Text = g.KaydedenID.ToString();
            textBox6.Text = g.KayitTarihi.ToString();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form1 c = new Form1();
            this.Hide();
            c.Show();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
